use pik16_odv_Query

GO

create table department_history(
 id int identity(1,1) primary key,
 event_name varchar(255),
 event_description varchar(1023),
 created_date datetime default(GETDATE())
);

create trigger department_INSERT
 on department after insert as
  insert into department_history (event_name, event_description)
   select N'insert', 'inserted new departmant: ' + name
   from inserted

insert into department values('department 10');

select * from department_history;

-- 2

create trigger department_DELETE
 on department after delete as
  insert into department_history (event_name, event_description)
   select N'delete', 'deleted departmant: ' + name
   from deleted

delete from department where name = 'department 10';

select * from department_history;

-- 3

create trigger department_UPDATE
 on department after update as
  insert into department_history (event_name, event_description)
   select N'update', 'new name for departmant id ' + CONVERT(varchar(10), id) + ': ' + name
   from inserted

update department set name = N'Updated department #10' where name = 'department 10';

select * from department_history;