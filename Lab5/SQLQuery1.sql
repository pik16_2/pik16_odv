use reiting

go

--IF NOT EXISTS(SELECT * FROM dbo_groups
--WHERE Kod_group = 'pik-16')
--	PRINT 'HI'

-----------------------------TRIGGER 5-----------------------------
create trigger lab6_insertStudendAddNewGroup
ON dbo_student
INSTEAD OF INSERT
AS

DECLARE
@def INT,
@Name varchar(25),
@Sname varchar(25),
@Fname varchar(25),
@N_ingroup INT,
@Kod_group varchar(25);

SELECT @def = 1;
SELECT @Name = ins.Name from inserted ins;
SELECT @Sname = ins.Sname from inserted ins;
SELECT @Fname = ins.Fname from inserted ins;
SELECT @N_ingroup = ins.N_ingroup from inserted ins;
SELECT @Kod_group = ins.Kod_group from inserted ins;

BEGIN
IF NOT EXISTS(
	SELECT * 
	FROM inserted
	WHERE inserted.Kod_group = ALL(
		SELECT dbo_student.Kod_group
		FROM dbo_student))
	INSERT INTO dbo_groups(Kod_group) values((SELECT inserted.Kod_group FROM inserted))
END
BEGIN
INSERT INTO dbo_student( Sname, Name, Fname, N_ingroup, Kod_group) 
VALUES(@Name, @Sname, @Fname, @N_ingroup, @Kod_group)
END
--------------------------------------------------------------------
INSERT INTO dbo_student(Kod_group) values('pik16')

	
DROP TRIGGER lab6_insertStudendAddNewGroup
--------------------------------------------------
-create trigger lab7_6
on dbo_student after update,insert,delete as
declare 
	@del_grup varchar(7);

Select @del_grup=ins.Kod_group from inserted ins

if exists(select Count(dbo_groups.kilk) from dbo_groups where dbo_groups.Kod_group=@del_grup)
	print 'normal'
else
	begin
		delete from dbo_groups where dbo_groups.Kod_group=@del_grup
	end
